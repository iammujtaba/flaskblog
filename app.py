from flask import Flask, render_template, session, request, logging, redirect, url_for, flash
from data import Articles
from flask_sqlalchemy import SQLAlchemy
from flask_mysqldb import MySQL
from passlib.hash import sha256_crypt
from wtforms import Form, StringField, IntegerField, TextAreaField, PasswordField, validators
app = Flask(__name__)

# Config MySQL
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'mohd'
app.config['MYSQL_PASSWORD'] = 'SURIYAKH1@'
app.config['MYSQL_DB'] = 'myflaskapp'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)

#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/wordcount_dev'
#db = SQLAlchemy(app)
#migrate = Migrate(app, db)


@app.route("/")
@app.route("/index")
def index():
    return render_template("home.html")


@app.route("/about")
def about():
    return render_template('about.html')


@app.route("/articles")
def articles():
    articles = Articles()
    return render_template("articles.html", articles=articles)


@app.route("/article/<string:myid>")
def article(myid):
    return render_template("article.html", id=myid)


@app.route('/login', methods=["GET", "POST"])
def login():
    if(request.method == "POST"):
        username = request.form['username']
        password = request.form['password']
        # Create Database Cursor
        curr = mysql.connection.cursor()
        result = curr.execute(
            "SELECT * FROM users WHERE username=%s", [username])
        if(result > 0):
            # Get stored hash pass
            data = curr.fetchone()
            print(data)
            data_password = data['password']
            # compare password
            if(sha256_crypt.verify(password, data_password)):
                session['logged_in'] = True
                session['username'] = username
                flash("You are now logged in,", 'success')
                return redirect(url_for('dashboard'))
            else:
                error = 'Wrong Password...'
                return render_template('login.html', error=error)
            # Connection Close..
            curr.close()
        else:
            error = 'Username Not Found (420Error)...'
            return render_template('login.html', error=error)

        return render_template('login.html')
    return render_template('login.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if(request.method == 'POST' and form.validate()):
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))

        # Cursor Create
        curr = mysql.connection.cursor()
        curr.execute("INSERT INTO users(name,emailid,username,password) VALUES(%s,%s,%s,%s)",
                     (name, email, username, password))

        # Commit to DB
        mysql.connection.commit()

        # Close COnnection

        flash('You are now registered and can log in', 'success')

        return redirect(url_for("login"))
    return render_template("register.html", form=form)


@app.route("/dashboard")
def dashboard():
    if(session.get('logged_in') == None):
        return redirect(url_for("error_dashboard"))
    return render_template('dashboard.html')


@app.route('/erorr_dashboard')
def error_dashboard():
    return render_template('logout.html')


@app.route('/logout')
def logout():
    session.clear()
    flash('Successfully Log Off from account! Login Again!', 'success')
    return redirect(url_for('login'))


@app.route('/add_article', methods=["GET", "POST"])
def add_article():
    
    form=ArticleForm(request.form)
    if(request.method=="POST" and form.validate()):
        title=form.title.data
        body=form.body.data
        curr=mysql.connection.cursor()
        curr.execute("INSERT INTO articles (title,author,body) values(%s,%s,%s)",(title,str(session['username']),str(body)))
        mysql.connection.commit()
        curr.close()
        
        flash("Article Added Successfully...",'success')
        return redirect(url_for("dashboard"))
    
    return render_template('add_article.html',form=form)
        
    


class RegistrationForm(Form):
    name = StringField('Name:', [validators.Length(min=1, max=50)])
    email = StringField('Email:', [validators.Length(min=6, max=90)])
    username = StringField('Username:', [validators.Length(min=4, max=30)])
    password = PasswordField('Password:', [validators.DataRequired(
    ), validators.equal_to('confirm_password', "Password do not match")])
    confirm_password = PasswordField('Confirm Password')


class LoginForm(Form):
    username = StringField('Username', [validators.Length(
        min=4, max=20), validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired()])


class ArticleForm(Form):
    title = StringField('Title:', [validators.Length(min=5, max=200)])
    body = TextAreaField('Body: ', [validators.Length(min=30)])


if(__name__ == "__main__"):
    app.secret_key = "secret_123"
    app.run(debug=True)
